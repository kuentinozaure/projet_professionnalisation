import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-professional-card-list',
  templateUrl: './professional-card-list.component.html',
  styleUrls: ['./professional-card-list.component.css']
})
export class ProfessionalCardListComponent implements OnInit {

  @Input() listOfCard: [];

  constructor() { }

  ngOnInit() {
    console.log(this.listOfCard)
  }

}
