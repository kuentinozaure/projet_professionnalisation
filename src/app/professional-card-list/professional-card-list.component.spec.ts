import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionalCardListComponent } from './professional-card-list.component';

describe('ProfessionalCardListComponent', () => {
  let component: ProfessionalCardListComponent;
  let fixture: ComponentFixture<ProfessionalCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessionalCardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionalCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
