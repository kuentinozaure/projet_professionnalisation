import { Component, OnInit } from '@angular/core';
import { firstBooster } from './mock-data/firstBooster';
import { secondBooster } from './mock-data/secondBooster';
import { thirdBooster } from './mock-data/thirdBooster';
import { fourthBooster } from './mock-data/fourthBooster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  selectedRoute: string;

  nbPoint: number;

  boosterDemarage = firstBooster;
  boosterDemarageTwo = secondBooster;
  boosterDemarageThird = thirdBooster;
  boosterDemarageFourth = fourthBooster;

  listOfBooster = [];

  listOfCardEarned = [];

  constructor() {
    this.listOfCardEarned = [];
    this.listOfBooster = [];
    this.selectedRoute = 'home';
    this.nbPoint = 0;
  }

  ngOnInit(): void {
    this.initBoosterList();
  }

  initBoosterList() {
    this.listOfBooster.push(this.boosterDemarage);
    this.listOfBooster.push(this.boosterDemarageTwo);
    this.listOfBooster.push(this.boosterDemarageThird);
    this.listOfBooster.push(this.boosterDemarageFourth);
  }


  changeRoute(routeSelected: string) {
    this.selectedRoute = routeSelected;
  }


  onBoosterSelected(boosterChanged: any) {
    boosterChanged.card.map((card: any) => {
      this.listOfCardEarned.push(card);
    });

    this.listOfBooster = this.listOfBooster.filter(booster => {
      return booster.id !== boosterChanged.id;
    });
    this.changeRoute('card_list');
  }

  onPointChanged(point: number) {
    this.nbPoint += point;
  }

  onPointLost(point: number) {
    this.nbPoint -= point;
  }

}
