export const quizzAnswer = [
  {
    id: 1,
    question: 'Qu elle est mon hobbie prefere ?',
    answers: [
      'tennis',
      'foot',
      'golf',
      'kanoe'
    ],
    good: 'tennis'
  },
  {
    id: 2,
    question: 'Qu elle est ma formation actuelle ?',
    answers: [
      'Master ICE',
      'Master Miage',
      'L3 miashs',
      'Pas de formation'
    ],
    good: 'Master ICE'
  },
  {
    id: 3,
    question: 'Ou est ce que j effectue mon alternance ?',
    answers: [
      'Sopra Steria',
      'Apple',
      'Air France',
      'Google'
    ],
    good: 'Air France'
  },
  {
    id: 4,
    question: 'Qu elle est ma qualité ?',
    answers: [
      'Innovant',
      'Colérique',
      'Adaptable',
      'Emotif'
    ],
    good: 'Innovant'
  },
  {
    id: 5,
    question: 'Ou est ce que j effectue ma formation ?',
    answers: [
      'Université toulouse Jean jaurés',
      'Paul sabatier',
      'Lycee Ozenne',
      'Capitole 1'
    ],
    good: 'Université toulouse Jean jaurés'
  },
  {
    id: 6,
    question: 'Qu elle est la technologie que j ai pu mettre en oeuvre dans un cadre personnelle ?',
    answers: [
      'VueJS avec VueX',
      'Java',
      'Python',
      'JS'
    ],
    good: 'VueJS avec VueX'
  },
  {
    id: 7,
    question: 'Qu elle est mon attentes personnelles ?',
    answers: [
      'Déménager a l étranger',
      'Rester dans son pays',
      'Ne rien decouvrir autour de lui',
      'Ne pas partager l informatique autour de lui'
    ],
    good: 'Déménager a l étranger'
  },
  {
    id: 8,
    question: 'Ou est ce que je souhaite déménager ?',
    answers: [
      'Paris',
      'Marseille',
      'Canada',
      'Bruxelles'
    ],
    good: 'Canada'
  },
  {
    id: 9,
    question: 'Qu elle est une de mes volonté professionnelle ? ',
    answers: [
      'Avoir des cheque resto',
      'Gagner beaucoup d argent',
      'Pouvoir etre acteur de mon entreprise',
      'Avoir une cantine professionnelle'
    ],
    good: 'Pouvoir etre acteur de mon entreprise'
  },
];
