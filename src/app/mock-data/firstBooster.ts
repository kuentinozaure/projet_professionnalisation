export const firstBooster = {
  id: 1,
  name: 'pack de demarage',
  price: 10,
  image: '../assets/millenium_pack.png',
  card: [
     {
      image: '../assets/MES_ATTENTES_PERSO_CARD.png'
    },
     {
      image: '../assets/MES_ATTENTES_PRO.png'
    }
  ],
};
