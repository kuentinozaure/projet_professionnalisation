import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyBoosterComponent } from './buy-booster.component';

describe('BuyBoosterComponent', () => {
  let component: BuyBoosterComponent;
  let fixture: ComponentFixture<BuyBoosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyBoosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyBoosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
