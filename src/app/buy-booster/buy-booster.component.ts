import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-buy-booster',
  templateUrl: './buy-booster.component.html',
  styleUrls: ['./buy-booster.component.css']
})
export class BuyBoosterComponent implements OnInit {

  @Input() listOfBooster: [];
  @Input() nbPoint: number;
  @Output() boosterChoosed = new EventEmitter<any>();
  @Output() pointLost = new EventEmitter<any>();


  boosterSelected: any;

  constructor(private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    this.boosterSelected = null;
  }

  getSelectedCard(booster: any) {
    this.boosterSelected = booster;
    if (this.checkIfUserCanBuyPack(booster)) {
      this.boosterChoosed.emit(booster);
      this.pointLost.emit(booster.price);
    }
  }

  checkIfUserCanBuyPack(boosterSelected: any) {
    return this.nbPoint >= boosterSelected.price;
  }

}
