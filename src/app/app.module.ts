import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProfessionalCardListComponent } from './professional-card-list/professional-card-list.component';
import { EarnPointComponent } from './earn-point/earn-point.component';
import { BuyBoosterComponent } from './buy-booster/buy-booster.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfessionalCardListComponent,
    EarnPointComponent,
    BuyBoosterComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
