import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { quizzAnswer } from '../mock-data/quizz-answer';

@Component({
  selector: 'app-earn-point',
  templateUrl: './earn-point.component.html',
  styleUrls: ['./earn-point.component.css']
})
export class EarnPointComponent implements OnInit {

  @Output() point = new EventEmitter<number>();

  questions = quizzAnswer;

  valueChoosedByUser: string;

  selectedQuestions: { id: number; question: string; answers: string[]; good: string; };

  constructor() {
    this.selectedQuestions = this.questions[this.getRandomInt(this.questions.length)];
    this.valueChoosedByUser = '';
  }

  ngOnInit() {
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  clickOnResponse(value: string) {
    console.log(value)
    this.valueChoosedByUser = value;

    if (this.checkIfCorrect() === true) {
      this.point.emit(10);
    }
  }

  checkIfCorrect() {
    return this.valueChoosedByUser === this.selectedQuestions.good;
  }

}
